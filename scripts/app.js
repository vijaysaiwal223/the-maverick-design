TweenMax.from(".me", 1, {
  delay: 0.4,
  y: 16,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from(".heading_text", 1, {
  delay: 0.6,
  y: 16,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from(".intro", 1, {
  delay: 0.8,
  y: 16,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from(".cta_wrapper", 1, {
  delay: 1.0,
  y: 16,
  opacity: 0,
  ease: Expo.easeInOut,
});

// gsap.to(".company-logo", { rotation: 360, x: 100, duration: 1 });

// Function to check the screen width and apply animation if necessary
function checkScreenWidth() {
  // Get the screen width
  var screenWidth =
    window.innerWidth ||
    document.documentElement.clientWidth ||
    document.body.clientWidth;

  // Get the element with the desired class
  var element = document.querySelector(".company-logo");

  // Check if the screen width is 375px or less
  if (screenWidth <= 375) {
    // Apply the animation to the element

    element.classList.add("logo-animation");
  } else {
    // Remove the animation class if screen width is greater than 375px
    element.classList.remove("logo-animation");
  }
}

// Initial check when the page loads
checkScreenWidth();

// Check again when the window is resized
window.addEventListener("resize", checkScreenWidth);

gsap.set(".logo-animation", {
  x: (i) => i * 200,
});

gsap.to(".logo-animation", {
  duration: 10,
  ease: "none",
  x: "-=400", //move each box 1480px to right
  modifiers: {
    x: gsap.utils.unitize((x) => parseFloat(x) % 400), //force x value to be between 0 and 1480 using modulus
  },
  repeat: -1,
});
