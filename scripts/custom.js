function animateMind() {
  const mind = ["🧠", "🧠", "🧠", "🧠", "🧠"];

  const eye = ["👀", "👀", "👀", "👀", "👀", "👀"];

  mind.forEach((emoji) => {
    const emojiElement = document.createElement("span");
    emojiElement.innerText = emoji;
    emojiElement.classList.add("emoji");

    const x = event.clientX - 25;
    const y = event.clientY - 25;
    emojiElement.style.left = x + "px";
    emojiElement.style.top = y + "px";

    const tx = Math.random() * 400 - 200 + "px";
    const ty = Math.random() * 400 - 200 + "px";
    emojiElement.style.setProperty("--tx", tx);
    emojiElement.style.setProperty("--ty", ty);

    document.body.appendChild(emojiElement);
    setTimeout(() => {
      emojiElement.remove();
    }, 1000);
  });
}

function animateEye() {
  const eye = ["👀", "👀", "👀", "👀", "👀", "👀"];
  eye.forEach((emoji) => {
    const emojiElement = document.createElement("span");
    emojiElement.innerText = emoji;
    emojiElement.classList.add("emoji");

    const x = event.clientX - 25;
    const y = event.clientY - 25;
    emojiElement.style.left = x + "px";
    emojiElement.style.top = y + "px";

    const tx = Math.random() * 400 - 200 + "px";
    const ty = Math.random() * 400 - 200 + "px";
    emojiElement.style.setProperty("--tx", tx);
    emojiElement.style.setProperty("--ty", ty);

    document.body.appendChild(emojiElement);
    setTimeout(() => {
      emojiElement.remove();
    }, 1000);
  });
}
